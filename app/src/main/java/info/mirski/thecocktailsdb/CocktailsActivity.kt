package info.mirski.thecocktailsdb

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.ui.Modifier
import dagger.hilt.android.AndroidEntryPoint
import info.mirski.thecocktailsdb.ui.CocktailsApp
import info.mirski.thecocktailsdb.ui.theme.TheCocktailsDBTheme

@AndroidEntryPoint
class CocktailsActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TheCocktailsDBTheme {
                CocktailsApp(modifier = Modifier.fillMaxSize())
            }
        }
    }
}