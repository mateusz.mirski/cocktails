package info.mirski.thecocktailsdb

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import info.mirski.thecocktailsdb.model.CocktailsRepository
import info.mirski.thecocktailsdb.model.Drink
import kotlinx.coroutines.launch


open class FavoritesModel(cocktailsRepository: CocktailsRepository) : ViewModel() {

    var favorites by mutableStateOf(emptyMap<String, Drink>())
        private set

    init {
        viewModelScope.launch {
            launch {
                snapshotFlow { cocktailsRepository.favorites }.collect {
                    favorites = it
                }
            }
        }
    }

}