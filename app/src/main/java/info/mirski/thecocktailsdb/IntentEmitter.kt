package info.mirski.thecocktailsdb

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import info.mirski.thecocktailsdb.model.Drink
import info.mirski.thecocktailsdb.ui.home.Page

const val KeyPage: String = "Page"
const val KeyDrink: String = "Drink"
const val KeyQuery: String = "Query"

val RefreshCocktailOfTheDay = Intent("RefreshCocktailOfTheDay")
val AddToFavorites = Intent("AddToFavorites")
val AddToDrinks = Intent("AddToDrinks")
val RemoveFromFavorites = Intent("RemoveFromFavorites")
val Search = Intent("Search")
val ShowDrinkDetails = Intent("ShowDrinkDetails")
val HomePage = Intent("HomePage")

val CocktailDetailsFilter = IntentFilter(ShowDrinkDetails.action)

val CocktailsRepositoryFilter = IntentFilter().apply {
    addAction(RefreshCocktailOfTheDay.action)
    addAction(AddToFavorites.action)
    addAction(AddToDrinks.action)
    addAction(RemoveFromFavorites.action)
    addAction(Search.action)
}

val HomePageFilter = IntentFilter().apply {
    addAction(Search.action)
    addAction(HomePage.action)
}

fun emitPage(context: Context, page: Page) {
    context.emit(HomePage.putExtra(KeyPage, page))
}

fun emitShowDrink(context: Context, drink: Drink) {
    context.emit(AddToDrinks.putExtra(KeyDrink, drink))
    context.emit(ShowDrinkDetails.putExtra(KeyDrink, drink))
}

fun emitSearch(context: Context, query: String) {
    context.emit(Search.putExtra(KeyQuery, query))
}

fun emitRefreshCocktailOfTheDay(context: Context) {
    context.emit(RefreshCocktailOfTheDay)
}

fun emitAddFavorite(context: Context, drink: Drink) {
    context.emit(AddToDrinks.putExtra(KeyDrink, drink))
    context.emit(AddToFavorites.putExtra(KeyDrink, drink))
}

fun emitRemoveFavorite(context: Context, drink: Drink) {
    context.emit(RemoveFromFavorites.putExtra(KeyDrink, drink))
}

private fun Context.emit(intent: Intent) {
    LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
}