package info.mirski.thecocktailsdb.datasource

import info.mirski.thecocktailsdb.db.DrinksDb
import info.mirski.thecocktailsdb.db.dao.DrinksDao
import info.mirski.thecocktailsdb.db.dao.FavoritesDao
import info.mirski.thecocktailsdb.db.entities.DrinkEntity
import info.mirski.thecocktailsdb.db.entities.FavoriteEntity
import info.mirski.thecocktailsdb.model.Drink
import info.mirski.thecocktailsdb.model.Ingredient
import javax.inject.Inject
import javax.inject.Singleton
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

@Singleton
class DrinksDataSourceDb @Inject constructor(
    private val drinksDb: DrinksDb
) {
    private val drinksDao: DrinksDao
        get() = drinksDb.drinksDao()

    private val favoritesDao: FavoritesDao
        get() = drinksDb.favoritesDao()

    suspend fun addDrink(drink: Drink) = drinksDao.insertAll(drink.toEntity())

    suspend fun removeDrink(drink: Drink) = drinksDao.delete(drink.toEntity())

    suspend fun getDrinkById(id: String): Drink? = drinksDao.getById(id)?.firstOrNull()?.toDrink()

    suspend fun addToFavorites(id: String) = favoritesDao.insertAll(FavoriteEntity(id))

    suspend fun remFromFavorites(id: String) = favoritesDao.delete(FavoriteEntity(id))

    fun getFavoriteDrinks(): Flow<List<Drink>> = drinksDao.getFavoriteDrinks().map { it.map(DrinkEntity::toDrink) }
}

fun DrinkEntity.toDrink(): Drink = Drink(
    id,
    name,
    glass,
    category,
    type,
    uri,
    ingredients.mapIndexed { index, ingredient -> Ingredient(ingredient, measures[index]) },
)

fun Drink.toEntity(): DrinkEntity {

    return DrinkEntity(
        id,
        name,
        glass,
        category,
        type,
        uri,
        ingredients.map { it.name },
        ingredients.map { it.quantity }
    )
}


