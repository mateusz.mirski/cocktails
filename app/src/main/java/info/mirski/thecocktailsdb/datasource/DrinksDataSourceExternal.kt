package info.mirski.thecocktailsdb.datasource

import android.util.Log
import info.mirski.thecocktailsdb.TAG
import info.mirski.thecocktailsdb.model.Drink
import info.mirski.thecocktailsdb.networking.CocktailsResponse
import info.mirski.thecocktailsdb.networking.CocktailsService
import javax.inject.Inject
import javax.inject.Singleton
import kotlinx.coroutines.CancellationException
import retrofit2.Response

@Singleton
class DrinksDataSourceExternal @Inject constructor(
    private val service: CocktailsService
) {
    suspend fun getDrinkById(id: String) = safeCall { service.getById(id) }?.firstOrNull()

    suspend fun getRandomDrink(): Drink? = safeCall { service.random() }?.firstOrNull()

    suspend fun search(query: String): List<Drink>? = safeCall { service.search(query) }?.allDrinks()

    private suspend fun safeCall(call: suspend () -> Response<CocktailsResponse>?): Response<CocktailsResponse>? =
        try {
            call()
        } catch (cancellationException: CancellationException) {
            throw cancellationException
        } catch (exception: Exception) {
            Log.e(TAG, "Exception", exception)
            null
        }
}

private fun Response<CocktailsResponse>.firstOrNull() =
    if (isSuccessful) {
        body()?.drinks?.firstOrNull()
    } else {
        null
    }?.let(::Drink)

private fun Response<CocktailsResponse>.allDrinks() =
    if (isSuccessful) {
        body()?.drinks
    } else {
        null
    }?.map(::Drink)
