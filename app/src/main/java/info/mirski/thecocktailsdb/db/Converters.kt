package info.mirski.thecocktailsdb.db

import android.net.Uri
import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


class Converters {

    @TypeConverter
    fun uriToString(uri: Uri): String? = if (uri == Uri.EMPTY) null else uri.toString()

    @TypeConverter
    fun stringToUri(url: String?): Uri = url?.let { Uri.parse(it) } ?: Uri.EMPTY


    @TypeConverter
    fun listToJson(list: List<String>): String {
        return Gson().toJson(list)
    }

    @TypeConverter
    fun jsonToList(json: String): List<String> {
        val listType: Type = object : TypeToken<List<String>>() {}.type
        return Gson().fromJson(json, listType)
    }

}