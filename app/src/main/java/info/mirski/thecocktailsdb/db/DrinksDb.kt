package info.mirski.thecocktailsdb.db

import androidx.room.Database
import androidx.room.DatabaseConfiguration
import androidx.room.InvalidationTracker
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteOpenHelper
import info.mirski.thecocktailsdb.db.dao.DrinksDao
import info.mirski.thecocktailsdb.db.dao.FavoritesDao
import info.mirski.thecocktailsdb.db.entities.DrinkEntity
import info.mirski.thecocktailsdb.db.entities.FavoriteEntity

@Database(entities = [DrinkEntity::class, FavoriteEntity::class], version = 1)
@TypeConverters(Converters::class)
abstract class DrinksDb : RoomDatabase() {
    abstract fun drinksDao(): DrinksDao
    abstract fun favoritesDao(): FavoritesDao
}

val FakeDb = object : DrinksDb() {
    override fun drinksDao(): DrinksDao {
        TODO("Not yet implemented")
    }

    override fun favoritesDao(): FavoritesDao {
        TODO("Not yet implemented")
    }

    override fun clearAllTables() {
        TODO("Not yet implemented")
    }

    override fun createInvalidationTracker(): InvalidationTracker {
        TODO("Not yet implemented")
    }

    override fun createOpenHelper(config: DatabaseConfiguration): SupportSQLiteOpenHelper {
        TODO("Not yet implemented")
    }

}
