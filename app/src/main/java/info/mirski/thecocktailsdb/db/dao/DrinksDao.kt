package info.mirski.thecocktailsdb.db.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import info.mirski.thecocktailsdb.db.entities.DrinkEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface DrinksDao {

    @Query("SELECT * FROM drinks")
    fun getAll(): Flow<List<DrinkEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg drinks: DrinkEntity)

    @Delete
    suspend fun delete(drink: DrinkEntity)

    @Query("SELECT * FROM drinks WHERE id LIKE :id")
    suspend fun getById(id: String): List<DrinkEntity>?

    @Query("Select * FROM drinks INNER JOIN favorites ON favorites.id = drinks.id")
    fun getFavoriteDrinks(): Flow<List<DrinkEntity>>
}
