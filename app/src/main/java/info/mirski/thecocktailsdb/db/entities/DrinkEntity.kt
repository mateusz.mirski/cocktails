package info.mirski.thecocktailsdb.db.entities

import android.net.Uri
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "drinks")
data class DrinkEntity(
    @PrimaryKey val id: String,
    val name: String,
    val glass: String,
    val category: String,
    val type: String,
    val uri: Uri,
    val ingredients: List<String>,
    val measures: List<String>,
)