package info.mirski.thecocktailsdb.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import info.mirski.thecocktailsdb.db.DrinksDb
import info.mirski.thecocktailsdb.networking.CocktailsService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Provides
    fun provideContext(@ApplicationContext context: Context): Context = context

    @Provides
    fun provideRetrofitService(): CocktailsService = Retrofit.Builder()
        .baseUrl("https://www.thecocktaildb.com/api/json/v1/1/")
        .addConverterFactory(GsonConverterFactory.create())
        .build().create(CocktailsService::class.java)

    @Provides
    fun provideDb(@ApplicationContext context: Context): DrinksDb {
        return Room.databaseBuilder(context, DrinksDb::class.java, "drinks-db").build()
    }
}