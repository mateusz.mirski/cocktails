package info.mirski.thecocktailsdb.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Qualifier
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers


enum class DispatcherScope {
    IO, Main, Default
}

@Suppress("unused")
@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class Dispatcher(val scope: DispatcherScope)

@InstallIn(SingletonComponent::class)
@Module
object DispatchersModule {
    @Provides
    @Dispatcher(DispatcherScope.IO)
    fun providesIODispatcher(): CoroutineDispatcher = Dispatchers.IO

    @Provides
    @Dispatcher(DispatcherScope.Main)
    fun providesMainDispatcher(): CoroutineDispatcher = Dispatchers.Main

    @Provides
    @Dispatcher(DispatcherScope.Default)
    fun providesDefaultDispatcher(): CoroutineDispatcher = Dispatchers.Default
}