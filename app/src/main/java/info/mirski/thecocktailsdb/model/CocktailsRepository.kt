package info.mirski.thecocktailsdb.model

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import info.mirski.thecocktailsdb.AddToDrinks
import info.mirski.thecocktailsdb.AddToFavorites
import info.mirski.thecocktailsdb.CocktailsRepositoryFilter
import info.mirski.thecocktailsdb.KeyDrink
import info.mirski.thecocktailsdb.KeyQuery
import info.mirski.thecocktailsdb.RefreshCocktailOfTheDay
import info.mirski.thecocktailsdb.RemoveFromFavorites
import info.mirski.thecocktailsdb.Search
import info.mirski.thecocktailsdb.datasource.DrinksDataSourceDb
import info.mirski.thecocktailsdb.datasource.DrinksDataSourceExternal
import info.mirski.thecocktailsdb.di.Dispatcher
import info.mirski.thecocktailsdb.di.DispatcherScope.IO
import info.mirski.thecocktailsdb.model.RequestState.Companion.Failed
import info.mirski.thecocktailsdb.model.RequestState.Failed
import javax.inject.Inject
import javax.inject.Singleton
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

sealed interface RequestState {
    companion object {
        val Failed: RequestState
            get() = Failed()
    }

    data class Success<T>(val response: T) : RequestState
    data class Failed(val message: String? = null) : RequestState
    data object InProgress : RequestState
}

@Singleton
class CocktailsRepository @Inject constructor(
    private val drinksDataSourceExternal: DrinksDataSourceExternal,
    private val drinksDataSourceDb: DrinksDataSourceDb,
    @Dispatcher(IO) private val dispatcher: CoroutineDispatcher,
    context: Context
) {
    private val broadcast = LocalBroadcastManager.getInstance(context)

    var favorites by mutableStateOf(mapOf<String, Drink>())
        private set

    var searchResult by mutableStateOf(Failed)
        private set

    var randomDrinkResult by mutableStateOf(Failed)
        private set

    init {
        broadcast.registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                when (intent.action) {
                    RefreshCocktailOfTheDay.action -> {
                        updateRandomDrink()
                    }

                    AddToFavorites.action -> {
                        intent.getDrinkExtra()?.let(::addToFavorites)
                    }

                    AddToDrinks.action -> {
                        intent.getDrinkExtra()?.let(::addToDrinks)
                    }

                    RemoveFromFavorites.action -> {
                        intent.getDrinkExtra()?.let(::remFromFavorites)
                    }

                    Search.action -> {
                        intent.getStringExtra(KeyQuery)?.let(::searchDrink)
                    }
                }
            }
        }, CocktailsRepositoryFilter)

        CoroutineScope(dispatcher).launch {
            launch {
                drinksDataSourceDb.getFavoriteDrinks().collect {
                    val pairs = it.map { drink -> Pair(drink.id, drink) }
                    favorites = linkedMapOf(*pairs.toTypedArray())
                }
            }
        }

        updateRandomDrink()
    }

    private fun addToFavorites(drink: Drink) {
        CoroutineScope(dispatcher).launch {
            drinksDataSourceDb.addToFavorites(drink.id)
        }
    }

    private fun remFromFavorites(drink: Drink) {
        CoroutineScope(dispatcher).launch {
            drinksDataSourceDb.remFromFavorites(drink.id)
        }
    }

    private fun addToDrinks(drink: Drink) {
        CoroutineScope(dispatcher).launch {
            drinksDataSourceDb.addDrink(drink)
        }
    }

    private fun searchDrink(query: String) {
        if (query.isNotEmpty()) {
            searchResult = RequestState.InProgress
            CoroutineScope(dispatcher).launch {
                searchResult = drinksDataSourceExternal.search(query)?.let { results ->
                    RequestState.Success(results.map { it })
                } ?: Failed()
            }
        }
    }

    private fun updateRandomDrink() {
        randomDrinkResult = RequestState.InProgress
        CoroutineScope(dispatcher).launch {
            randomDrinkResult = drinksDataSourceExternal.getRandomDrink()?.let {
                RequestState.Success(it)
            } ?: Failed
        }
    }

    suspend fun getDrinkById(id: String): Drink? =
        drinksDataSourceDb.getDrinkById(id) ?: drinksDataSourceExternal.getDrinkById(id)?.apply {
            drinksDataSourceDb.addDrink(this)
        }
}

fun Intent.getDrinkExtra() = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
    getParcelableExtra(KeyDrink, Drink::class.java)
} else {
    @Suppress("DEPRECATION") getParcelableExtra(KeyDrink) as? Drink
}

