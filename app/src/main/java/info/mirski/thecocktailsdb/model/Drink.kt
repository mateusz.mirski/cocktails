package info.mirski.thecocktailsdb.model

import android.net.Uri
import android.os.Parcelable
import info.mirski.thecocktailsdb.networking.CocktailData
import kotlinx.parcelize.Parcelize

@Parcelize
data class Ingredient(val name: String, val quantity: String) : Parcelable {
    companion object {
        fun create(ingredient: String?, measure: String?) = if (ingredient.isNullOrEmpty() || measure.isNullOrEmpty()) {
            null
        } else {
            Ingredient(ingredient, measure)
        }
    }
}

@Parcelize
data class Drink(
    val id: String,
    val name: String,
    val glass: String,
    val category: String,
    val type: String,
    val uri: Uri,
    val ingredients: List<Ingredient> = emptyList(),
) : Parcelable {
    val thumbUri: Uri
        get() = if (uri == Uri.EMPTY) uri
        else Uri.parse("$uri/preview")

    constructor(drink: CocktailData) : this(
        drink.idDrink ?: "",
        drink.strDrink ?: "NoName",
        drink.strGlass ?: "NoGlass",
        drink.strCategory ?: "NoCategory",
        drink.strAlcoholic ?: "Unknown",
        drink.strDrinkThumb?.let { url -> Uri.parse(url.toString()) } ?: Uri.EMPTY,
        drink.getIngredientsList(),
    )

    companion object {
        val Default: Drink = Drink(
            "dbId",
            "My Best Drink Ever",
            "Bigg",
            "TheBestOf",
            "AsYouWish",
            Uri.parse("https://en.wikipedia.org/wiki/Flag_of_Blackbeard#/media/File:Pirate_Flag_of_Blackbeard_(Edward_Teach).svg"),
            ingredients = listOf(
                Ingredient("Żabie języki", "6 pc."),
                Ingredient("Włos czarownicy", "1"),
                Ingredient("Sok z jagód zbieranych w świetle księżyca", "5 oz")
            )
        )
    }
}

private fun CocktailData.getIngredientsList() = listOfNotNull(
    Ingredient.create(strIngredient1, strMeasure1),
    Ingredient.create(strIngredient2, strMeasure2),
    Ingredient.create(strIngredient3, strMeasure3),
    Ingredient.create(strIngredient4, strMeasure4),
    Ingredient.create(strIngredient5, strMeasure5),
    Ingredient.create(strIngredient6, strMeasure6),
    Ingredient.create(strIngredient7, strMeasure7),
    Ingredient.create(strIngredient8, strMeasure8),
    Ingredient.create(strIngredient9, strMeasure9),
    Ingredient.create(strIngredient10, strMeasure10),
    Ingredient.create(strIngredient11, strMeasure11),
    Ingredient.create(strIngredient12, strMeasure12),
    Ingredient.create(strIngredient13, strMeasure13),
    Ingredient.create(strIngredient14, strMeasure14),
    Ingredient.create(strIngredient15, strMeasure15)
)
