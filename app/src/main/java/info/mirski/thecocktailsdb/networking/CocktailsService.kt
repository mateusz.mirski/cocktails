package info.mirski.thecocktailsdb.networking

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CocktailsService {

    companion object {
        val Preview: CocktailsService = object : CocktailsService {
            override suspend fun random(): Response<CocktailsResponse> {

                return Response.success(CocktailsResponse(emptyList()))
            }

            override suspend fun search(query: String): Response<CocktailsResponse> {
                return Response.success(CocktailsResponse(emptyList()))
            }

            override suspend fun getById(id: String): Response<CocktailsResponse> {
                return Response.success(CocktailsResponse(emptyList()))
            }

        }
    }

    @GET("random.php")
    suspend fun random(): Response<CocktailsResponse>

    @GET("search.php")
    suspend fun search(@Query("s") query: String): Response<CocktailsResponse>

    @GET("lookup.php")
    suspend fun getById(@Query("i") id: String): Response<CocktailsResponse>
}
