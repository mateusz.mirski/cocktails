package info.mirski.thecocktailsdb.ui

import android.content.res.Configuration
import android.net.Uri
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.material.icons.filled.ShoppingCart
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Start
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalInspectionMode
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import info.mirski.thecocktailsdb.R
import info.mirski.thecocktailsdb.emitAddFavorite
import info.mirski.thecocktailsdb.emitRemoveFavorite
import info.mirski.thecocktailsdb.model.Drink
import info.mirski.thecocktailsdb.ui.theme.TheCocktailsDBTheme

@Composable
fun CocktailScreen(
    drinkId: String,
    modifier: Modifier = Modifier,
    model: CocktailScreenModel = hiltViewModel()
) {
    LaunchedEffect(key1 = drinkId) {
        model.getDrink(drinkId)
    }

    val drink = model.drink
    if (drink != null) {
        CocktailScreen(drink, model.favorites.contains(drink.id), modifier)
    } else {
        CircularProgressIndicator(modifier = modifier)
    }
}

@Composable
fun CocktailScreen(
    drink: Drink,
    isFavorite: Boolean,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = Modifier
            .padding(vertical = 16.dp)
            .fillMaxSize()
            .then(modifier),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        val context = LocalContext.current

        Icon(
            modifier = Modifier
                .clickable {
                    if (isFavorite) {
                        emitRemoveFavorite(context, drink)
                    } else {
                        emitAddFavorite(context, drink)
                    }
                }
                .align(Alignment.End)
                .padding(8.dp),
            imageVector = if (isFavorite) Icons.Default.Favorite else Icons.Default.FavoriteBorder,
            contentDescription = null
        )

        Text(
            modifier = Modifier.padding(horizontal = 24.dp),
            text = drink.name,
            fontSize = TextUnit(32f, TextUnitType.Sp)
        )

        Image(drink.uri)

        Text(text = stringResource(R.string.category, drink.category))

        Text(text = stringResource(R.string.glass, drink.glass))

        Text(text = stringResource(R.string.drink_type, drink.type))

        Text(
            modifier = Modifier.padding(vertical = 8.dp),
            text = stringResource(R.string.ingredients)
        )
        drink.ingredients.forEach {
            Row(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .padding(horizontal = 16.dp)
            ) {

                Text(text = it.name)

                Spacer(Modifier.weight(1f))

                Text(text = it.quantity)
            }

        }


        Spacer(modifier = Modifier.weight(1f))

        Text(
            modifier = Modifier.align(Start),
            text = stringResource(R.string.drink_id, drink.id),
            fontSize = TextUnit(12f, TextUnitType.Sp)
        )
    }
}


@Composable
fun Image(uri: Uri, contentDescription: String? = null) = if (LocalInspectionMode.current) {
    Icon(
        modifier = Modifier.size(128.dp),
        imageVector = Icons.Default.ShoppingCart,
        contentDescription = contentDescription
    )
} else {
    AsyncImage(
        model = uri,
        contentDescription = contentDescription,
    )
}

@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun CocktailScreenPreview() {
    TheCocktailsDBTheme {
        Scaffold {
            CocktailScreen(
                modifier = Modifier.padding(it),
                isFavorite = true,
                drink = Drink.Default
            )
        }
    }
}