package info.mirski.thecocktailsdb.ui

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import info.mirski.thecocktailsdb.FavoritesModel
import info.mirski.thecocktailsdb.model.CocktailsRepository
import info.mirski.thecocktailsdb.model.Drink
import javax.inject.Inject
import kotlinx.coroutines.launch

@HiltViewModel
open class CocktailScreenModel @Inject constructor(
    private val cocktailsRepository: CocktailsRepository
) : FavoritesModel(cocktailsRepository) {

    var drink by mutableStateOf<Drink?>(null)
        private set

    fun getDrink(id: String) {
        viewModelScope.launch {
            drink = cocktailsRepository.getDrinkById(id)
        }
    }
}
