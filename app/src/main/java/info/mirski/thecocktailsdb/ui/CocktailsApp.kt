package info.mirski.thecocktailsdb.ui

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import info.mirski.thecocktailsdb.CocktailDetailsFilter
import info.mirski.thecocktailsdb.model.getDrinkExtra
import info.mirski.thecocktailsdb.ui.home.HomeScreen
import info.mirski.thecocktailsdb.ui.theme.TheCocktailsDBTheme

enum class Routes {
    Home, DrinkDetails
}

@Composable
fun CocktailsApp(
    modifier: Modifier,
    navController: NavHostController = rememberNavController()
) {
    RegisterForIntents(navController)

    NavHost(navController = navController, startDestination = Routes.Home) {
        composable(Routes.Home.name) {
            HomeScreen(modifier)
        }

        composable("${Routes.DrinkDetails.name}/{id}") { stack ->
            stack.arguments?.getString("id")?.let { id ->
                Scaffold {
                    CocktailScreen(modifier = Modifier.padding(it), drinkId = id)
                }
            }
        }
    }
}

@Composable
fun RegisterForIntents(navController: NavHostController) {
    val context = LocalContext.current
    val receiver = remember(navController) {
        object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                intent.getDrinkExtra()?.let { drink ->
                    navController.navigate(Routes.DrinkDetails, drink.id)
                }
            }
        }
    }

    LaunchedEffect(key1 = navController, key2 = receiver) {
        LocalBroadcastManager.getInstance(context).registerReceiver(
            receiver, CocktailDetailsFilter
        )
    }

    DisposableEffect(key1 = navController, key2 = receiver) {
        onDispose {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver)
        }
    }
}

fun NavHostController.navigate(route: Routes, id: String) {
    navigate("${route.name}/$id")
}

@Composable
fun NavHost(navController: NavHostController, startDestination: Routes, builder: NavGraphBuilder.() -> Unit) =
    NavHost(navController = navController, startDestination = startDestination.name, builder = builder)

@Preview
@Composable
fun PreviewApp() {
    CocktailsApp(modifier = Modifier.fillMaxSize())
}

@Composable
fun PreviewFrame(body: @Composable (innerPadding: PaddingValues) -> Unit) {
    TheCocktailsDBTheme {
        Scaffold(
            topBar = { SearchForDrinks() }
        ) {
            body(it)
        }
    }
}