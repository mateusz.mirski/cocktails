package info.mirski.thecocktailsdb.ui

import android.content.Context
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import info.mirski.thecocktailsdb.emitShowDrink
import info.mirski.thecocktailsdb.model.Drink

@Composable
fun DrinksList(modifier: Modifier = Modifier, drinks: List<Drink> = emptyList(), favorites: Set<String> = emptySet(), context: Context = LocalContext.current) {
    LazyColumn(modifier = modifier) {
        items(drinks.size) {
            val drink = drinks[it]
            Row(
                modifier = Modifier
                    .clickable {
                        emitShowDrink(context, drink)
                    }
                    .background(color = MaterialTheme.colorScheme.primary.copy(alpha = 1f / ((it % 3) + 1)))
                    .padding(16.dp)
                    .fillMaxWidth()
            ) {
                AsyncImage(model = drink.thumbUri, contentDescription = "thumb")
                Text(drink.name)
                if (favorites.contains(drink.id)) {
                    Spacer(modifier = Modifier.weight(1f))
                    Icon(imageVector = Icons.Default.Favorite, contentDescription = "Favorite Cocktail")
                }
            }
        }
    }
}