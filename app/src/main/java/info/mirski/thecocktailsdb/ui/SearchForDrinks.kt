package info.mirski.thecocktailsdb.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Icon
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import info.mirski.thecocktailsdb.R
import info.mirski.thecocktailsdb.ui.theme.TheCocktailsDBTheme

@Composable
fun SearchForDrinks(initQuery: String = "", onQuery: (String) -> Unit = {}) {
    var query by remember { mutableStateOf(initQuery) }

    TextField(
        modifier = Modifier.fillMaxWidth(),
        leadingIcon = {
            Icon(imageVector = Icons.Default.Search, contentDescription = stringResource(R.string.search_field))
        },
        trailingIcon = {
            Icon(
                modifier = Modifier.clickable {
                    query = ""
                    onQuery("")
                },
                imageVector = Icons.Default.Clear,
                contentDescription = "Clear query"
            )
        },
        value = query,
        onValueChange = {
            query = it
            onQuery(it)
        })
}

@Preview
@Composable
fun SearchForDrinksPreview() {
    TheCocktailsDBTheme {
        SearchForDrinks("sialala") {}
    }
}