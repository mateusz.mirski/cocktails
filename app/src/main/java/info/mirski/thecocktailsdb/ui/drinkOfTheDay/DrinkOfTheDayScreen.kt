package info.mirski.thecocktailsdb.ui.drinkOfTheDay

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import info.mirski.thecocktailsdb.R
import info.mirski.thecocktailsdb.emitRefreshCocktailOfTheDay
import info.mirski.thecocktailsdb.model.Drink
import info.mirski.thecocktailsdb.model.RequestState
import info.mirski.thecocktailsdb.ui.CocktailScreen
import info.mirski.thecocktailsdb.ui.PreviewFrame
import java.util.Collections.singletonMap

@Composable
fun DrinkOfTheDay(
    modifier: Modifier,
    model: DrinkOfTheDayScreenModel = hiltViewModel()
) {
    DrinkOfTheDay(modifier, model.randomDrink, model.favorites)
}

@Composable
fun DrinkOfTheDay(
    modifier: Modifier = Modifier,
    drinkState: RequestState,
    favorites: Map<String, Drink>,
) {
    val context = LocalContext.current
    Box(modifier = modifier.fillMaxSize()) {
        when (drinkState) {
            is RequestState.Failed -> Button(
                onClick = { emitRefreshCocktailOfTheDay(context) }) {
                Text(text = stringResource(R.string.refresh))
            }

            RequestState.InProgress -> CircularProgressIndicator()

            is RequestState.Success<*> -> (drinkState.response as? Drink)?.apply { CocktailScreen(this, favorites.contains(id)) }
        }
    }
}

@Preview
@Composable
fun DrinkOfTheDayPreview() {
    PreviewFrame {
        DrinkOfTheDay(
            modifier = Modifier.padding(it),
            drinkState = RequestState.Success(Drink.Default),
            favorites = emptyMap()
        )
    }
}

@Preview
@Composable
fun DrinkOfTheDayInFavoritesPreview() {
    PreviewFrame {
        DrinkOfTheDay(
            modifier = Modifier.padding(it),
            drinkState = RequestState.Success(Drink.Default),
            favorites = singletonMap(Drink.Default.id, Drink.Default)
        )
    }
}

@Preview
@Composable
fun DrinkOfTheDayPreviewInProgress() {
    PreviewFrame {
        DrinkOfTheDay(
            modifier = Modifier.padding(it),
            drinkState = RequestState.InProgress,
            favorites = emptyMap()
        )
    }
}

@Preview
@Composable
fun DrinkOfTheDayPreviewFailed() {
    PreviewFrame {
        DrinkOfTheDay(
            modifier = Modifier.padding(it),
            drinkState = RequestState.Failed,
            favorites = emptyMap()
        )
    }
}
