package info.mirski.thecocktailsdb.ui.drinkOfTheDay

import dagger.hilt.android.lifecycle.HiltViewModel
import info.mirski.thecocktailsdb.FavoritesModel
import info.mirski.thecocktailsdb.model.CocktailsRepository
import info.mirski.thecocktailsdb.model.RequestState
import javax.inject.Inject

@HiltViewModel
class DrinkOfTheDayScreenModel @Inject constructor(
    private val cocktailsRepository: CocktailsRepository
) : FavoritesModel(cocktailsRepository) {
    val randomDrink: RequestState
        get() = cocktailsRepository.randomDrinkResult
}
