package info.mirski.thecocktailsdb.ui.favorites

import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import info.mirski.thecocktailsdb.model.Drink
import info.mirski.thecocktailsdb.ui.DrinksList
import info.mirski.thecocktailsdb.ui.PreviewFrame

@Composable
fun FavoritesScreen(
    modifier: Modifier,
    model: FavoritesScreenModel = hiltViewModel()
) = FavoritesScreen(modifier = modifier, model.favorites)

@Composable
fun FavoritesScreen(
    modifier: Modifier,
    drinks: Map<String, Drink>
) = DrinksList(modifier = modifier, drinks = drinks.values.toList(), favorites = drinks.keys)

@Preview
@Composable
fun FavoritesScreenPreview() {
    val drinks = mapOf(
        Pair(Drink.Default.id, Drink.Default),
        Drink.Default.copy(id = "123").let { Pair(it.id, it) },
        Drink.Default.copy(id = "128").let { Pair(it.id, it) }
    )

    PreviewFrame {
        FavoritesScreen(modifier = Modifier.padding(it), drinks)
    }
}
