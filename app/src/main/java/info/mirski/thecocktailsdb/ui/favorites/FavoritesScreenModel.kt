package info.mirski.thecocktailsdb.ui.favorites

import dagger.hilt.android.lifecycle.HiltViewModel
import info.mirski.thecocktailsdb.FavoritesModel
import info.mirski.thecocktailsdb.model.CocktailsRepository
import javax.inject.Inject

@HiltViewModel
class FavoritesScreenModel @Inject constructor(cocktailsRepository: CocktailsRepository) : FavoritesModel(cocktailsRepository)