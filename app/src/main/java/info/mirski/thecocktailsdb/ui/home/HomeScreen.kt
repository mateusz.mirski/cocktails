package info.mirski.thecocktailsdb.ui.home

import android.content.Context
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.material.icons.filled.ThumbUp
import androidx.compose.material.icons.outlined.ThumbUp
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalInspectionMode
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import info.mirski.thecocktailsdb.emitPage
import info.mirski.thecocktailsdb.emitRefreshCocktailOfTheDay
import info.mirski.thecocktailsdb.emitSearch
import info.mirski.thecocktailsdb.model.Drink
import info.mirski.thecocktailsdb.model.RequestState
import info.mirski.thecocktailsdb.ui.SearchForDrinks
import info.mirski.thecocktailsdb.ui.drinkOfTheDay.DrinkOfTheDay
import info.mirski.thecocktailsdb.ui.favorites.FavoritesScreen
import info.mirski.thecocktailsdb.ui.search.SearchResultsScreen
import info.mirski.thecocktailsdb.ui.theme.TheCocktailsDBTheme
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@Composable
fun HomeScreen(
    modifier: Modifier,
    model: HomeScreenModel = hiltViewModel(),
) = HomeScreen(
    modifier = modifier,
    model.page,
    model.searchQuery,
)

@Composable
fun HomeScreen(
    modifier: Modifier,
    page: Page,
    searchQuery: String,
    context: Context = LocalContext.current,
) {

    val searchBar = @Composable {
        val coroutineScope = rememberCoroutineScope()
        var job: Job? = remember { null }

        SearchForDrinks(searchQuery) {
            job?.cancel()
            job = coroutineScope.launch {
                delay(700)
                emitSearch(context, it)
            }
        }
    }


    Scaffold(
        modifier = modifier,
        topBar = searchBar,
        bottomBar = {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceEvenly
            ) {
                val size = 48.dp //TODO: create dimens
                Icon(
                    modifier = Modifier
                        .requiredSize(size)
                        .clickable(onClick = { onDrinkPageClicked(page, context) }),
                    imageVector = if (page == Page.DrinkOfTheDay) Icons.Default.ThumbUp else Icons.Outlined.ThumbUp,
                    contentDescription = "TipOfTheDay"
                )
                Icon(
                    modifier = Modifier
                        .requiredSize(size)
                        .clickable { emitPage(context, Page.Favorites) },
                    imageVector = if (page == Page.Favorites) Icons.Default.Favorite else Icons.Default.FavoriteBorder,
                    contentDescription = "Favorites"
                )
            }
        }
    ) { innerPadding ->
        if (LocalInspectionMode.current) {
            LocalInspectionPages(page, innerPadding)
        } else {
            when (page) {
                Page.DrinkOfTheDay -> DrinkOfTheDay(modifier = Modifier.padding(innerPadding))
                Page.SearchResult -> SearchResultsScreen(modifier = Modifier.padding(innerPadding)) { emitSearch(context, searchQuery) }
                Page.Favorites -> FavoritesScreen(Modifier.padding(innerPadding))
            }
        }
    }
}

fun onDrinkPageClicked(page: Page, context: Context) {
    if (page == Page.DrinkOfTheDay) {
        emitRefreshCocktailOfTheDay(context)
    } else {
        emitPage(context, Page.DrinkOfTheDay)
    }
}

@Composable
private fun LocalInspectionPages(page: Page, innerPadding: PaddingValues) {
    when (page) {
        Page.DrinkOfTheDay -> DrinkOfTheDay(modifier = Modifier.padding(innerPadding), drinkState = RequestState.Success(Drink.Default), favorites = emptyMap())
        Page.SearchResult -> {}//TODO SearchResultsScreen(modifier = Modifier.padding(innerPadding))
        Page.Favorites -> {
            val drinks = mapOf(
                Pair(Drink.Default.id, Drink.Default),
                Drink.Default.copy(id = "123").let { Pair(it.id, it) },
                Drink.Default.copy(id = "128").let { Pair(it.id, it) }
            )

            FavoritesScreen(Modifier.padding(innerPadding), drinks = drinks)
        }
    }
}


@Preview
@Composable
fun HomeScreenPreview() {
    TheCocktailsDBTheme {
        HomeScreen(
            modifier = Modifier,
            page = Page.DrinkOfTheDay,
            searchQuery = "Margarita"
        )
    }
}

@Preview
@Composable
fun HomeScreenPreviewFavorites() {


    TheCocktailsDBTheme {
        HomeScreen(
            modifier = Modifier,
            page = Page.Favorites,
            searchQuery = "Margarita"
        )
    }
}