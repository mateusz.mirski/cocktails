package info.mirski.thecocktailsdb.ui.home

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import dagger.hilt.android.lifecycle.HiltViewModel
import info.mirski.thecocktailsdb.HomePage
import info.mirski.thecocktailsdb.HomePageFilter
import info.mirski.thecocktailsdb.KeyPage
import info.mirski.thecocktailsdb.KeyQuery
import info.mirski.thecocktailsdb.Search
import info.mirski.thecocktailsdb.model.CocktailsRepository
import javax.inject.Inject
import kotlinx.coroutines.launch

enum class Page {
    DrinkOfTheDay, SearchResult, Favorites
}

@HiltViewModel
class HomeScreenModel @Inject constructor(private val cocktailsRepository: CocktailsRepository, context: Context) : ViewModel() {
    var page by mutableStateOf(Page.DrinkOfTheDay)
        private set

    var searchQuery by mutableStateOf("")
        private set

    init {
        LocalBroadcastManager.getInstance(context).registerReceiver(
            object : BroadcastReceiver() {
                override fun onReceive(context: Context, intent: Intent) {
                    when (intent.action) {
                        Search.action -> {
                            searchQuery = intent.getStringExtra(KeyQuery) ?: ""
                            page = if (searchQuery.isNotEmpty()) {
                                Page.SearchResult
                            } else {
                                Page.Favorites
                            }
                        }

                        HomePage.action -> {
                            page = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                                intent.getSerializableExtra(KeyPage, Page::class.java)
                            } else {
                                (intent.getSerializableExtra(KeyPage) as? Page)
                            } ?: Page.DrinkOfTheDay
                        }
                    }
                }
            }, HomePageFilter
        )

        viewModelScope.launch {
            snapshotFlow { cocktailsRepository.favorites }.collect {
                if (it.isNotEmpty()) page = Page.Favorites
            }
        }
    }
}
