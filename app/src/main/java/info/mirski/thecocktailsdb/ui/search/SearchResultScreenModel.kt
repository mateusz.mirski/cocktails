package info.mirski.thecocktailsdb.ui.search

import dagger.hilt.android.lifecycle.HiltViewModel
import info.mirski.thecocktailsdb.FavoritesModel
import info.mirski.thecocktailsdb.model.CocktailsRepository
import info.mirski.thecocktailsdb.model.RequestState
import javax.inject.Inject


@HiltViewModel
open class SearchResultScreenModel @Inject constructor(
    private val cocktailsRepository: CocktailsRepository
) : FavoritesModel(cocktailsRepository) {

    open val searchResult: RequestState
        get() = cocktailsRepository.searchResult
}