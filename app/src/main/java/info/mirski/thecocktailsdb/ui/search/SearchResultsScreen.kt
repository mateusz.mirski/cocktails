package info.mirski.thecocktailsdb.ui.search

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import info.mirski.thecocktailsdb.R
import info.mirski.thecocktailsdb.model.Drink
import info.mirski.thecocktailsdb.model.RequestState
import info.mirski.thecocktailsdb.ui.DrinksList
import info.mirski.thecocktailsdb.ui.PreviewFrame


@Composable
fun SearchResultsScreen(
    modifier: Modifier,
    model: SearchResultScreenModel = hiltViewModel(),
    onSearchRefresh: () -> Unit
) {
    SearchResultsScreen(modifier, model.searchResult, model.favorites.keys, onSearchRefresh)
}

@Composable
fun SearchResultsScreen(
    modifier: Modifier,
    searchResult: RequestState,
    favorites: Set<String>,
    onSearchRefresh: () -> Unit = {}
) {
    Box(modifier = modifier.fillMaxSize()) {
        when (searchResult) {
            is RequestState.Failed -> {
                Button(onClick = { onSearchRefresh }) {
                    Text(text = stringResource(R.string.refresh))
                }
            }

            RequestState.InProgress -> CircularProgressIndicator()

            is RequestState.Success<*> -> {
                val list = searchResult.response as? List<*>
                val drinks = list?.filterIsInstance<Drink>() ?: emptyList()
                DrinksList(drinks = drinks, favorites = favorites)
            }
        }
    }
}

@Preview
@Composable
fun SearchResultScreenPreview() {
    PreviewFrame {
        val drinks = listOf(Drink.Default, Drink.Default.copy(id = "33", name = "Poison"))
        val result = RequestState.Success(drinks)
        SearchResultsScreen(modifier = Modifier.padding(it), searchResult = result, setOf("33"))
    }
}

@Preview
@Composable
fun SearchResultScreenPreviewInProgress() {
    PreviewFrame {
        SearchResultsScreen(modifier = Modifier.padding(it), searchResult = RequestState.InProgress, emptySet())
    }
}

@Preview
@Composable
fun SearchResultScreenFailed() {
    PreviewFrame {
        SearchResultsScreen(modifier = Modifier.padding(it), searchResult = RequestState.Failed, emptySet())
    }
}