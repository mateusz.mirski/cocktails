package info.mirski.thecocktailsdb.datasource


import android.net.Uri
import android.util.Log
import info.mirski.thecocktailsdb.model.Drink
import info.mirski.thecocktailsdb.networking.CocktailData
import info.mirski.thecocktailsdb.networking.CocktailsResponse
import info.mirski.thecocktailsdb.networking.CocktailsService
import java.net.URL
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Assert.assertNull
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.any
import org.mockito.Mockito.anyString
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.kotlin.doReturn
import org.robolectric.RobolectricTestRunner
import retrofit2.Response

@RunWith(RobolectricTestRunner::class)
class DrinksDataSourceExternalTest {
    private val serviceMock = mock(CocktailsService::class.java)

    @Test
    fun `test getById call exception`() {
        Mockito.mockStatic(Log::class.java).use { log ->
            log.`when`<Any> { Log.e(anyString(), anyString(), any<Throwable>()) }.doReturn(1)
            runTest {
                `when`(serviceMock.getById(anyString())).thenThrow(RuntimeException("Exception test"))
                val dataSourceExternal = DrinksDataSourceExternal(serviceMock)
                val drink = dataSourceExternal.getDrinkById("1234")
                assertNull(drink)
            }
        }
    }

    @Test
    fun `test random call exception`() {
        Mockito.mockStatic(Log::class.java).use { log ->
            log.`when`<Any> { Log.e(anyString(), anyString(), any<Throwable>()) }.doReturn(1)
            runTest {
                `when`(serviceMock.random()).thenThrow(RuntimeException("Exception test"))
                val dataSourceExternal = DrinksDataSourceExternal(serviceMock)
                val drink = dataSourceExternal.getRandomDrink()
                assertNull(drink)
            }
        }
    }

    @Test
    fun `test query call exception`() {
        Mockito.mockStatic(Log::class.java).use { log ->
            log.`when`<Any> { Log.e(anyString(), anyString(), any<Throwable>()) }.doReturn(1)
            runTest {
                `when`(serviceMock.search(anyString())).thenThrow(RuntimeException("Exception test"))
                val dataSourceExternal = DrinksDataSourceExternal(serviceMock)
                val drink = dataSourceExternal.search(query = "query")
                assertNull(drink)
            }
        }
    }

    @Test
    fun `test getById call Success`() {
        val resList = listOf(cocktail)
        Mockito.mockStatic(Uri::class.java).use { uri ->
            uri.`when`<Uri> { Uri.parse(anyString()) }.doReturn(Uri.EMPTY)

            Mockito.mockStatic(Log::class.java).use { log ->
                log.`when`<Any> { Log.e(anyString(), anyString(), any<Throwable>()) }.doReturn(1)
                runTest {
                    `when`(serviceMock.getById(anyString())).thenReturn(Response.success(CocktailsResponse(resList)))
                    val dataSourceExternal = DrinksDataSourceExternal(serviceMock)
                    val drink = dataSourceExternal.getDrinkById("1234")

                    assertEquals(Drink(cocktail), drink)
                    assertNotEquals(Drink(cocktail2), drink)
                }
            }
        }
    }
}

val cocktail = CocktailData(
    "", "", "", "", "", URL(Drink.Default.uri.toString())
)
val cocktail2 = CocktailData(
    "213", "sdfs", "", "", "", URL(Drink.Default.uri.toString())
)